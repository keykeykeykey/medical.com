<html>
<header>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/reset.css" rel="stylesheet" type="text/css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script src="js/jquery-1.11.3.js" type="application/javascript"></script>


    <!--    <script src="js/myjs.js" type="application/javascript"></script>-->
</header>
<body>
<?php
require_once 'class/Decompression.php';
require_once 'class/WriteSuggest.php';
$status_types = array (
    "易感" => array (  "低于平均水平",
        "平均水平",
        "高于平均水平"),
    "天赋" => array (  "天赋较低",
        "有天赋",
        "天赋较高"),
);
$titlename2=Decompression::read_files($title);
$title='';
$titles=$titlename2["titlename"];
$titles=array_splice($titles,1);
//var_dump($titles);
array_shift($titles);
//var_dump($titles);
foreach($titles as $title=> $status){
    if($title!=''&&$title!=null){


    $title=iconv('GBK', 'UTF-8', $title);
    $titlename=Decompression::read_files($title);
    $status=iconv('GBK', 'UTF-8', $status);
if(strpos($status,"天赋")==0){
    $type="运动能力";
    $status_choose=$status_types["天赋"];
//    $type=iconv('UTF-8', 'GBK', "运动能力");
}else{
    $type="基因易感水平";
    $status_choose=$status_types["易感"];
//    $type=iconv('UTF-8', 'GBK', "基因易感水平");
}
$page_id=$title.$status;
$table_content=iconv('GBK', 'UTF-8', $titlename["content"]);
$rows=explode("\n",$table_content);
$suggest=WriteSuggest::get("suggest");
?>
        <script type="application/javascript">
            $(function(){
                var status_choose="<?php echo $page_id; ?>";
                console.log(status_choose);
                if(status_choose.indexOf('高')>=0){
                    console.log(status_choose.indexOf('高'));
                    $(".<?php echo $page_id; ?>").find("li").eq(2).find(".click").css("display","block");
                }else if(status_choose.indexOf('低')>=0){
                    console.log(status_choose.indexOf('低'));
                    console.log(status_choose.indexOf('高'));
                    $(".<?php echo $page_id; ?>").find("li").eq(0).find(".click").css("display","block");
                }else{
                    $(".<?php echo $page_id; ?>").find("li").eq(1).find(".click").css("display","block");

                }
            })
        </script>
<div class="container">
    <div class="model1">
        <h3>一、检测结果评价</h3>
        <div >通过检测相关基因位点，您的<?php echo $title.$type ?>为：</div>
        <ul class="<?php echo $page_id; ?>" >
            <?php
            foreach($status_choose as $value){
                ?>
                <li><?php echo $value; ?><span class="click">√</span></li>

                <?php
            }
            ?>
        </ul>
        <div class="explation"><b>结果解释：</b>与人群平均水平相比，
            先天基因上，您的<?php echo $type;?>属于<?php  echo $status?>。
            但是，您后天所处的环境和生活习惯也将影响到您的运动能力。
            请参照下面的建议，用积极预防的态度，提高能力。</div>
    </div>
    <div class="model2">
        <h3>二、检测指标列表</h3>
        <table cellspacing="0"cellpadding="0">
            <?php
            foreach ($rows as $row){
                if($row!=""&&$row!=null){
                    $cols=explode("\t",$row);
                    ?>
                    <tr>
                        <?php
                        foreach($cols as $col){
                            ?>
                            <td><?php echo $col;//iconv('GBK', 'UTF-8', $col) ?></td>
                            <?php
                        }
                        ?>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>
    </div>
    <div class="model3">
        <h3>三、综合建议</h3>
        <ul class="suggest">
            <?php
            foreach($suggest->医生建议 as $key=> $suggests){
                ?>
                <li><?php echo ($key+1).'.'.$suggests; ?></li>
                <?php
            }
            ?>
        </ul>
    </div>
</div>
        <?php
    }
}
?>
</body>
</html>

