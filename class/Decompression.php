<?php
class Decompression
{
    protected $filename;

    function __construct($filename)
    {
        $this->filename = $filename;
    }
    
    /*
     * 解压文件
     * */
    function uncompress(){
        $zip = new ZipArchive();
        $file = @$zip->open($this->filename);
//        var_dump($this->filename);
//        var_dump($file);
        if($file) {
//            var_dump($zip->numFiles);
            for($i = 0; $i < $zip->numFiles; ++$i) {
                $item = $zip->statIndex($i);
//                var_dump($item);
                // 忽略目录
                if(!$item['size']) continue;

//                $export_url = './unziped/'.iconv('GBK', 'UTF-8', $item['name']);
                $export_url = 'unziped/'.$item['name'];
                $dirname = dirname($export_url);
                if(!is_dir($dirname)) {
                    mkdir($dirname, 0777, true);
                }
//                var_dump(iconv('UTF-8', 'GBK', $item['name']));
                @file_put_contents($export_url, $zip->getFromIndex($i));
            }
        } else {
            die("解压文件失败！");
        }

    }

    /*
     * 遍历解压出来的文件
    */
    static function read_files($filetitle=null){
        $result=array();
        $files=scandir("./unziped/");
        foreach($files as $filename){
            if($filename!="."&&$filename!=".."){
                $file_inside=scandir("./unziped/".$filename);
                foreach($file_inside as $file_inside_name){
                    if(strpos($file_inside_name,".xls")){
                        $titlename=self::getMaintable($filename,$file_inside_name);
                        $result['titlename']=$titlename;
                    }elseif($file_inside_name!="."&&$file_inside_name!=".."){
                        $url="./unziped/".$filename.'/'.$file_inside_name;
//                        $filetitle=iconv('UTF-8', 'GBK', $filetitle);
                        $result['content']= self::create_tables($url,$filetitle);
                    }
                }
            }
        }
        return $result;
    }
    /*
     * 获得总表中所有的检验项目
     * */
    static function getMaintable($filename,$file_inside_name){
        $titlename=self::read_mainxsl("./unziped/".$filename.'/'.$file_inside_name);
        return $titlename;
    }
    /*
     * 读取testSample_Risk_Estimation.xls
     * */
    static function read_mainxsl($url){
        $text = file_get_contents($url);
        $rows = explode("\n", $text);
        $titlename=[];
        foreach($rows as $row) {
            $titlename[explode("\t", $row)[0]]=explode("\t", $row)[2];
        }
        return $titlename;
    }
    /*
     * 获取每一个项目的具体数据
     * */
     static function create_tables($url,$filetitle){
         if($filetitle==null){
         }else{
             $files=scandir($url);
             foreach($files as $file){
                 if($file){
                     $filefullname=$filetitle."_gene_info.xls";
//                     var_dump($filefullname);
                     $filefullname = iconv('UTF-8', 'GBK', $filetitle)."_gene_info.xls";
                     if($file==$filefullname){
                         $file_content=file_get_contents($url.'/'.$filefullname);
                         return $file_content;
                     }
                 }
             }
         }
    }
}